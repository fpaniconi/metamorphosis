﻿using UnityEngine;
using System.Collections;

public class WaitAndActive : MonoBehaviour {

	AudioSource aSource;
	float fadeTime = 2;
	// Use this for initialization
	void Start () {
		Invoke("load",6);
		aSource = GameObject.Find("Camera").GetComponent<AudioSource>();

		Invoke("load",8);
	}

	// Update is called once per frame
	void load () {
		GetComponent<LevelLoader>().enabled=true;

		GetComponent<LevelLoader>().JustfaidOut();
		Invoke("LoadLevel",2);
	}

	void LoadLevel()
	{

		iTween.ValueTo(gameObject, iTween.Hash("from", 1.0f, "to", 0.0f,"time", fadeTime, "easetype", "linear","onupdate", "setVolumen", "oncomplete","LoadLevelFinal"));	

	}

	void setVolumen(float newVol)
	{
		print(newVol);
		aSource.volume=newVol;
	}

	void LoadLevelFinal()
	{
		GetComponent<LevelLoader>().LoadLevelOnClick();
	}

}
